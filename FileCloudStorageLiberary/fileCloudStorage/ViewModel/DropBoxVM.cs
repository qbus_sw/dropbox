﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FileCloudStorageLiberary;
namespace fileCloudStorage.ViewModel
{
    public class DropBoxVM
    {

        private DropBox MyDropBox { get; set; }

        public DropBoxVM()
        {
            MyDropBox = new DropBox();

        }

        public void SetReturnUri(string returnUri)
        {
            MyDropBox.RedirectUri = returnUri;
        }

        public string returnConnectString()
        {
            return MyDropBox.ConnectDropBoxString();

        }


    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;
using FileCloudStorageLiberary;
using fileCloudStorage.ViewModel;
namespace fileCloudStorage.Controllers
{
    public class HomeController : Controller
    {

        public DropBoxVM ViewModelDropBox { get; set; }
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult ConnectCloudStorage()
        {


            return View();
        }

        
        public IActionResult ConnectingDropBox()
        {
            ViewModelDropBox = new DropBoxVM();
            ViewModelDropBox.SetReturnUri("http://localhost/DropBoxAuthorized");
            string Uri = ViewModelDropBox.returnConnectString();
            return Redirect(Uri);
        }

        public IActionResult DropBoxAuthorized()
        {

            return Redirect("DropBoxConnected");
        }

        public IActionResult DropBoxConnected()
        {

            return View();
        }


        public IActionResult Error()
        {
            return View();
        }
    }
}

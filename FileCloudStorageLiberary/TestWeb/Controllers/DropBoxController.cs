﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;
using Microsoft.AspNet.Http;
using Microsoft.AspNet.Hosting;
using TestWeb.ViewModel;
using TestWeb.Model;
using Newtonsoft.Json;
using System.Net;
using System.IO;
using Microsoft.Net.Http.Headers;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace TestWeb.Controllers
{
    public class DropBoxController : Controller
    {
        IHostingEnvironment _enviroment;
        public DropBoxController(IHostingEnvironment env)
        {
            _enviroment = env;
        }
        public DropBoxVM DBVM { get; set; }

        // GET: /<controller>/
        public IActionResult Index()
        {
            return View();
        }
        public IActionResult Authorize()
        {
            DBVM = new DropBoxVM();
            DBVM.SetRedirectUri("http://localhost:5000/DropBox/AuthorizeResult/");
            string uri = DBVM.GeefUri();
            return Redirect(uri);
        }


        [HttpGet]
        public IActionResult AuthorizeResult(string code, string state)
        {

            var jsonResp = FileCloudStorageLiberary.DropBox.GetAccesToken(code, "http://localhost:5000/DropBox/AuthorizeResult/");
            string url =string.Format( "http://localhost:5000/DropBox/AcountPage?access_token={0}&bearer={1}&uid{2}", jsonResp["access_token"], jsonResp["token_type"], jsonResp["uid"]);
            return Redirect(url);
            
        }


       

       

        public async Task<IActionResult> AcountPage(string access_token, string bearer,string uid)
        {
            DBVM = new DropBoxVM();
            DBVM.AccesToken = access_token;
           HttpContext.Session.SetString("access_token", access_token);
            await DBVM.logIn();
            ViewData["OnSucces"] = false;
            return View(DBVM.MyAccount);
        }

        [HttpPost]
        public async Task<IActionResult> AcountPage(ICollection<IFormFile> files)
        {
            DBVM = new DropBoxVM();
            DBVM.AccesToken = HttpContext.Session.GetString("access_token");
            await DBVM.logIn();
            var path = Path.Combine(_enviroment.WebRootPath, "uploaded");
            foreach (var file in files)
            {
                if(file.Length >0)
                {
                    var filename = ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName.Trim('"');
                    var filepath = Path.Combine(path, @filename);
                    await file.SaveAsAsync(filepath);
                    await DBVM.UploadFile(filepath);
                    System.IO.File.Delete(filepath);

                }

            }
             ViewData["OnSucces"] = true;
            return View();

        }



    }
}

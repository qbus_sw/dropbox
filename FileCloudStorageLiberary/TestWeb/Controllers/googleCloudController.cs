﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;
using Microsoft.AspNet.Http;
using Microsoft.AspNet.Hosting;
using System.IO;
using Microsoft.Net.Http.Headers;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace TestWeb.Controllers
{
    public class googleCloudController : Controller
    {
        IHostingEnvironment _enviroment;
        public googleCloudController(IHostingEnvironment env)
        {
            _enviroment = env;
        }

        // GET: /<controller>/
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult GetCode()
        {
            FileCloudStorageLiberary.GoogleDrive drive = new FileCloudStorageLiberary.GoogleDrive();
            return Redirect(drive.GetAuthCodeUrl("http://localhost:5000/googleCloud/Auth2CallBack"));

        }

        public IActionResult Auth2CallBack(string code)
        {

            Dictionary<string, string> waardes = FileCloudStorageLiberary.GoogleDrive.GetAccesToken(code, "http://localhost:5000/googleCloud/Auth2CallBack");
            foreach(var key in waardes.Keys)
            {
                HttpContext.Session.SetString(key, waardes[key]);
            }
            
            return RedirectToAction("UploadFile");
        }

        public IActionResult UploadFile()
        {
            var parentId = FileCloudStorageLiberary.GoogleDrive.MaakCloudFolder(HttpContext.Session.GetString("access_token"));
            HttpContext.Session.SetString("ParentId", parentId);
            var lijst = FileCloudStorageLiberary.GoogleDrive.GetList(HttpContext.Session.GetString("access_token"), parentId);
            return View(lijst);

        }
        [HttpPost]
        public async Task<IActionResult> UploadFile(IEnumerable<IFormFile> files)
        {
            
            var path = Path.Combine(_enviroment.WebRootPath, "uploaded");
            foreach (var file in files)
            {
                if (file.Length > 0)
                {
                    var filename = ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName.Trim('"');
                    var filepath = Path.Combine(path, @filename);
                    await file.SaveAsAsync(filepath);
                    FileCloudStorageLiberary.GoogleDrive.UploadFile(HttpContext.Session.GetString("access_token"), filepath, file.ContentType, filename, HttpContext.Session.GetString("ParentId"));
                  
                    System.IO.File.Delete(filepath);

                }

            }
            var lijst = FileCloudStorageLiberary.GoogleDrive.GetList(HttpContext.Session.GetString("access_token"),HttpContext.Session.GetString("ParentId"));
            return View(lijst);
        }


    }
}

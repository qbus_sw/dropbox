﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;
using Microsoft.AspNet.Http;
using Microsoft.AspNet.Hosting;
using Microsoft.Net.Http.Headers;
using System.IO;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace TestWeb.Controllers
{
    public class OneDriveController : Controller
    {

        IHostingEnvironment _enviroment;
        public OneDriveController(IHostingEnvironment env)
        {
            _enviroment = env;
        }
        // GET: /<controller>/
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult AuthCode()
        {
          string uri =   FileCloudStorageLiberary.OneDrive.GetWebAuthUri("http://localhost:5000/OneDrive/Auth2CallBack");

            return Redirect(uri);
        }

        public IActionResult Auth2CallBack(string code)
        {
            FileCloudStorageLiberary.AccesCodeResponseOneDrive accescode = FileCloudStorageLiberary.OneDrive.GetAccesCode(code, "http://localhost:5000/OneDrive/Auth2CallBack");

            HttpContext.Session.SetString("access_token", accescode.access_token);
            HttpContext.Session.SetString("expires_in", accescode.expires_in);
            HttpContext.Session.SetString("time_granted", DateTime.Now.TimeOfDay.ToString());
            HttpContext.Session.SetString("refresh_token", accescode.refresh_token);

            return RedirectToAction("OverView");
        }

        public async Task<IActionResult> OverView()
        {
            var item =  await FileCloudStorageLiberary.OneDrive.AccData(HttpContext.Session.GetString("access_token"));
            return View(item);
        }

        public IActionResult UploadFile()
        {
            FileCloudStorageLiberary.OneDriveModels.FileLijstOneDrive lijst = FileCloudStorageLiberary.OneDrive.GetFiles(HttpContext.Session.GetString("access_token"));
            return View(lijst);
        }
        [HttpPost]
        public async Task<IActionResult> UploadFile(ICollection<IFormFile> files)
        {
            string acces_code = HttpContext.Session.GetString("access_token");

            var path = Path.Combine(_enviroment.WebRootPath, "uploaded");
            foreach (var file in files)
            {
                if (file.Length > 0)
                {
                    var filename = ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName.Trim('"');
                    var filepath = Path.Combine(path, @filename);
                    await file.SaveAsAsync(filepath);
                    await FileCloudStorageLiberary.OneDrive.UploadFile(acces_code, filepath, filename, file.Length);

                    System.IO.File.Delete(filepath);

                }

            }

            FileCloudStorageLiberary.OneDriveModels.FileLijstOneDrive lijst = FileCloudStorageLiberary.OneDrive.GetFiles(acces_code);
            return View(lijst);


        }
    }
}

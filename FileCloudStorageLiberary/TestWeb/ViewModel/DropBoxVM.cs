﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FileCloudStorageLiberary;
using TestWeb.Model;

namespace TestWeb.ViewModel
{
    public class DropBoxVM
    {
        private DropBox MyDropBox { get; set; }

        public void SetRedirectUri(string uri)
        {
            MyDropBox.RedirectUri = uri;
        }

        public DropBoxVM()
        {
            MyDropBox = new DropBox();
            _accesToken = "";
            MyDropBox.OnAccountConnected += MyDropBox_OnAccountConnected;
        }

        private Acount _myAccount;

        public Acount MyAccount
        {
            get { return _myAccount; }
            set { _myAccount = value; }
        }


        private void MyDropBox_OnAccountConnected()
        {
            MyAccount = new Acount() { Email = MyDropBox.UserEmail, Name = MyDropBox.UserName };
        }

        public string GeefUri()
        {
            return MyDropBox.ConnectDropBoxString();
        }

        private string _accesToken;

        public string AccesToken
        {
            get { return _accesToken; }
            set { _accesToken = value; }
        }

        public async Task logIn()
        {
           await  MyDropBox.ConnectWithAuth(AccesToken);
        }

        public async Task UploadFile(string filepath)
        {
            await MyDropBox.UploadFile(filepath);
        }

    }
}

﻿using System.ComponentModel;
using FileCloudStorageLiberary;
using System.Windows.Forms;

namespace TestAppStorage
{
    class MainWindowModel:INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }


        private DropBox _dropBox;

        public DropBox DropBox
        {
            get {
                if(_dropBox == null)
                {
                    _dropBox = new DropBox();
                    
                }
                return _dropBox; }
            set { _dropBox = value; }
        }


        private string _userName;

        public string UserName
        {
            get { return _userName; }
            set { _userName = value; OnPropertyChanged("UserName"); }
        }

        private string _email;

        public string Email
        {
            get { return _email; }
            set { _email = value; OnPropertyChanged("Email"); }
        }

        private string _filePath;

        public string FilePath
        {
            get { return _filePath; }
            set { _filePath = value; OnPropertyChanged("FilePath"); }
        }


        public void FileUploadPath()
        {
            OpenFileDialog file = new OpenFileDialog();
            file.Multiselect = false;
            DialogResult result = file.ShowDialog();
            if(result.HasFlag(DialogResult.OK))
            {
                FilePath = file.FileName;
            }



        }


        private bool _isConnected;

        public bool IsConnected
        {
            get { return _isConnected; }
            set { _isConnected = value; OnPropertyChanged("IsConnected"); }
        }


        private string _uploadStatus;

        public string UploadStatus
        {
            get { return _uploadStatus; }
            set { _uploadStatus = value; OnPropertyChanged("UploadStatus"); }
        }


        public MainWindowModel()
        {
            DropBox.OnAccountConnected += DropBox_OnAccountConnected;
            DropBox.OnUploadFinished += DropBox_OnUploadFinished;
            IsConnected = false;
        }

        private void DropBox_OnUploadFinished()
        {
            UploadStatus = "Finished";
        }

        private void DropBox_OnAccountConnected()
        {
            UserName = DropBox.UserName;
            Email = DropBox.UserEmail;
            IsConnected = true;

        }

        private static MainWindowModel _instance;

        public static MainWindowModel Instance
        {
            get {

                if(_instance == null)
                {
                    _instance = new MainWindowModel();
                }
                return _instance; }
            set { _instance = value; }
        }


        public void ConnectWithoutAuth()
        {

            DropBox.ConnectWithoutAuth();
            
        }

        public async void Upload()
        {
            if (FilePath != "")
            {
                UploadStatus = "Uploading..";
                await DropBox.UploadFile(FilePath);
            }

        }


    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace TestAppStorage
{
    /// <summary>
    /// Interaction logic for GoogleDriveWindow.xaml
    /// </summary>
    public partial class GoogleDriveWindow : Window
    {
        private GoogleDriveWindowModel model;
        public GoogleDriveWindow()
        {
            model = new GoogleDriveWindowModel();
            DataContext = model;
            InitializeComponent();
        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            model.BrowseFiles();
        }

        private void btnUpload_Click(object sender, RoutedEventArgs e)
        {
            model.Upload();
        }

        private void btnLogIn_Click(object sender, RoutedEventArgs e)
        {
            model.InitDrive();
        }
    }
}

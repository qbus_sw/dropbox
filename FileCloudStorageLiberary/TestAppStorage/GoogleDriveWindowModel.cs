﻿using System.Windows.Forms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using FileCloudStorageLiberary;
using System.IO;

namespace TestAppStorage
{
    class GoogleDriveWindowModel: INotifyPropertyChanged
    {

        private long _bytesSend;

        public long BytesSend
        {
            get { return _bytesSend; }
            set { _bytesSend = value; OnPropertyChanged("BytesSend"); }
        }

        private long _byteTotal;

        public long ByteTotal
        {
            get { return _byteTotal; }
            set { _byteTotal = value; OnPropertyChanged("ByteTotal"); }
        }

        public void CalculateBytes()
        {
            FileInfo info = new FileInfo(FilePath);
            ByteTotal = info.Length;
        }


        private string _filePath;

        public string FilePath
        {
            get { return _filePath; }
            set { _filePath = value; OnPropertyChanged("FilePath"); CanUpload = true; CalculateBytes(); }
        }

        private bool _canUpload;

        public bool CanUpload
        {
            get { return _canUpload; }
            set { _canUpload = value; OnPropertyChanged("CanUpload"); }
        }

        private bool _uploadSucces;

        public bool UploadSucces
        {
            get { return _uploadSucces; }
            set { _uploadSucces = value; }
        }

        private string _uploadStatus;

        public string UploadStatus
        {
            get { return _uploadStatus; }
            set { _uploadStatus = value; OnPropertyChanged("UploadStatus"); }
        }

        private string _googleEmail;

        public string GoogleEmail
        {
            get { return _googleEmail; }
            set { _googleEmail = value;  OnPropertyChanged("GoogleEmail"); }
        }



        #region Inotify
        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string proprtyName)
        {
            if(PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(proprtyName));
            }
        }
        #endregion

        public void BrowseFiles()
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Multiselect = false;
            DialogResult result = dialog.ShowDialog();
            if(result == DialogResult.OK)
            {
                FilePath = dialog.FileName;
            }

        }


        private GoogleDrive _myGoogleDrive;

       public GoogleDriveWindowModel()
        {
            _myGoogleDrive = new GoogleDrive();
            _myGoogleDrive.OnProgressChanged += _myGoogleDrive_OnProgressChanged;  
            _canUpload = false;
        }

        private void _myGoogleDrive_OnProgressChanged(long value)
        {
            BytesSend = value;
        }

        public void InitDrive()
        {
            _myGoogleDrive.AuthorizeGoogle(GoogleEmail);
            _myGoogleDrive.CreateService();
            _myGoogleDrive.MaakCloudMap();
        }

        public async void Upload()
        {
            UploadStatus = "uploading";
           UploadSucces =  await _myGoogleDrive.uploadFile(FilePath);
            SetUploadStatus();
        }

        void SetUploadStatus()
        {
            if(UploadSucces)
            {
                UploadStatus = "Upload Finished Succesful";
            }
            else
            {
                UploadStatus = "Upload Failed";
            }
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace TestAppStorage
{
    /// <summary>
    /// Interaction logic for universalWebBrowser.xaml
    /// </summary>
    public partial class universalWebBrowser : Window
    {
        public Uri returnUri;
        string uri;
        public universalWebBrowser(string url)
        {
            uri = url;
            InitializeComponent();
            web.Navigate(url);
        }

        private void web_Navigated(object sender, System.Windows.Navigation.NavigationEventArgs e)
        {
           if(e.Uri.AbsoluteUri != uri)
            {
                returnUri = e.Uri;
                Close();
            }

        }
    }
}

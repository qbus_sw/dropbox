﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace TestAppStorage
{
    /// <summary>
    /// Interaction logic for SelectWindow.xaml
    /// </summary>
    public partial class SelectWindow : Window
    {
        public SelectWindow()
        {
            InitializeComponent();
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            DropboxWindow window = new DropboxWindow();
            window.ShowDialog();
        }

        private void btnGoogleDrive_Click(object sender, RoutedEventArgs e)
        {
            GoogleDriveWindow window = new GoogleDriveWindow();
            window.ShowDialog();
        }

        private void btnOneDrive_Click(object sender, RoutedEventArgs e)
        {
            OneDriveWindow window = new OneDriveWindow();
            window.ShowDialog();
        }
    }
}

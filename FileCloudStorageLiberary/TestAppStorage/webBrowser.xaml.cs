﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Web;
namespace TestAppStorage
{
    /// <summary>
    /// Interaction logic for webBrowser.xaml
    /// </summary>
    public partial class webBrowser : Window
    {
        public webBrowser()
        {
            InitializeComponent();
            web.Navigate(MainWindowModel.Instance.DropBox.ConnectDropBoxString());
        }

        private void web_Navigated(object sender, System.Windows.Navigation.NavigationEventArgs e)
        {
                if(e.Uri.AbsoluteUri.StartsWith(MainWindowModel.Instance.DropBox.RedirectUri))
            {
                if (e.Uri.Fragment!= "")
                {
                    var accesToken = System.Web.HttpUtility.ParseQueryString(e.Uri.Fragment.Substring(1)).Get(0);

                    MainWindowModel.Instance.DropBox.ConnectWithAuth(accesToken);
                }
                this.Close();

            }
           
        }
    }
}

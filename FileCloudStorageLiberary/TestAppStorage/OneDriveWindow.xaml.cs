﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace TestAppStorage
{
    /// <summary>
    /// Interaction logic for OneDriveWindow.xaml
    /// </summary>
    public partial class OneDriveWindow : Window
    {
        OneDriveWindowModel model = new OneDriveWindowModel();
        public OneDriveWindow()
        {
            DataContext = model;
            InitializeComponent();
        }

        private void btnLogIn_Click(object sender, RoutedEventArgs e)
        {
            model.authenticateUser();
        }
    }
}

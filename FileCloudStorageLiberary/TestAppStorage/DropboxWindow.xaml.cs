﻿using FileCloudStorageLiberary;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace TestAppStorage
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class DropboxWindow : Window
    {
       

       
        


        public DropboxWindow()
        {
            this.DataContext = MainWindowModel.Instance;
          
            InitializeComponent();
            

        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            MainWindowModel.Instance.ConnectWithoutAuth();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            MainWindowModel.Instance.FileUploadPath();
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            MainWindowModel.Instance.Upload();
        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            Window win = new webBrowser();
            win.ShowDialog();
        }
    }
}

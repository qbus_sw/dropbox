﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.OneDrive.Sdk;
using System.Net;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace FileCloudStorageLiberary
{
  public  class OneDrive
    {
        private AccountSession _acc;

        public AccountSession Acc
        {
            get { return _acc; }
            set { _acc = value; }
        }

        private string UserName;

        public string _userName
        {
            get { return UserName; }
            set { UserName = value; }
        }

        



        private static string clientID = "0000000040185A2D";
        private static string clientSecret = "C0oogxLIvd153PTtdwnfP6u8YQ-9EdUn";


        public static async Task UploadFile(string AccesCode, string filepath, string filename,long contentsize)
        {
            ResumeOneDrive uri = CreateSession(AccesCode,filename);
            List<byte[]> chunks = await SplitData(filepath);
            await SendChunks(AccesCode, filename, chunks, uri, contentsize);
        }
        private static ResumeOneDrive CreateSession(string access_code,string filename)
        {
            HttpWebRequest req = (HttpWebRequest)HttpWebRequest.Create("https://api.onedrive.com/v1.0/drive/special/approot:/"+filename+":/upload.createSession?access_token=" + access_code);
            req.Method = "Post";
            req.ContentLength = 0;
            req.ContentType = "application/json";
            
            try {
                using (WebResponse resp = req.GetResponse())
                {
                    using (StreamReader reader = new StreamReader(resp.GetResponseStream()))
                    {
                        string json = reader.ReadToEnd();
                        ResumeOneDrive drive = JsonConvert.DeserializeObject<ResumeOneDrive>(json);
                        return drive;
                    }
                }
            }
            catch(WebException status)
            {
                return null;    
            }
        }

        private static async Task<List<byte[]>> SplitData(string filepath)
        {
            List<byte[]> Chunks = new List<byte[]>();
            
            byte[] chunk = new byte[8388608]; 
            using (FileStream stream = new FileStream(filepath, FileMode.Open))
            {
                if(stream.Length < 8388608)
                {
                    chunk = new byte[stream.Length];
                }
                int read = 0;
                while (read < stream.Length)
                {
                    if ((stream.Length - read) < 8388608)
                    {
                        chunk = new byte[(stream.Length - read)];
                        read += await stream.ReadAsync(chunk, 0, chunk.Length);
                    }
                    else
                    {
                        read += await stream.ReadAsync(chunk, 0, chunk.Length);
                    }
                    Chunks.Add(chunk);
                }

            }

            return Chunks;

        }

        public static async Task SendChunks(string access_token,string filename,List<byte[]> chunks,ResumeOneDrive resume,long size)
        {
            int StartNr = 0;
            foreach (byte[] chunk in chunks)
            {
                HttpWebRequest req = (HttpWebRequest)HttpWebRequest.Create(resume.uploadUrl + "?access_token=" + access_token);
                req.Method = "Put";
                req.ContentLength = chunk.Length;
                string range = "bytes " + StartNr + "-" + (StartNr + (chunk.Length - 1)) + "/" + size;
                req.Headers.Add("Content-Range",range );
                using (Stream stream = req.GetRequestStream())
                {
                   await stream.WriteAsync(chunk, 0, chunk.Length);
                }
                try {
                    using (WebResponse response = req.GetResponse())
                    {
                        using (StreamReader reader = new StreamReader(response.GetResponseStream()))
                        {
                            string json = reader.ReadToEnd();
                            
                        }

                    }

                  }
                
                 catch(WebException ex)
                {
            }

                    StartNr += (chunk.Length);
            }
        }




        public static dynamic GetFiles(string access_token)
        {
            HttpWebRequest req = (HttpWebRequest)HttpWebRequest.Create("https://api.onedrive.com/v1.0/drive/special/approot/children?access_token=" + access_token);
            try {
                using (WebResponse response = req.GetResponse())
                {
                    using (StreamReader reader = new StreamReader(response.GetResponseStream()))
                    {
                        string json = reader.ReadToEnd();
                        var lijst = JsonConvert.DeserializeObject<FileCloudStorageLiberary.OneDriveModels.FileLijstOneDrive>(json);
                        return lijst;
                    }

                }
            }
            catch(WebException ex)
            {
                return null;
            }
        }

        public static string GetWebAuthUri(string redirect)
        {
            string url;
          

            url = string.Format(@"https://login.live.com/oauth20_authorize.srf?client_id={0}&scope={1}&response_type=code&redirect_uri={2}", clientID, "wl.basic%20wl.offline_access%20wl.skydrive%20onedrive.appfolder", redirect);

            return url;
        }

        public static async Task<OneDriveAccount> AccData(string acces_token)
        {
            OneDriveAccount acc = new OneDriveAccount();
            try {
                HttpWebRequest req = (HttpWebRequest)HttpWebRequest.Create("https://apis.live.net/v5.0/me?access_token=" + acces_token);
                using (WebResponse response = req.GetResponse())
                {
                    using (StreamReader read = new StreamReader(response.GetResponseStream()))
                    {
                        string json = read.ReadToEnd();
                        acc = JsonConvert.DeserializeObject<OneDriveAccount>(json);

                    }
                }

                req = (HttpWebRequest)HttpWebRequest.Create("https://apis.live.net/v5.0/me/picture?access_token=" + acces_token+ "&suppress_redirects=true");
                using (WebResponse response = req.GetResponse())
                {
                    Stream stream = response.GetResponseStream();
                    using (StreamReader read = new StreamReader(stream))
                    {
                        char[] chars = new char[response.ContentLength];
                        int x = await read.ReadAsync(chars, 0, chars.Length);
                        string pic = new string(chars);
                        dynamic dyn = JsonConvert.DeserializeObject<dynamic>(pic);
                        acc.picture = dyn.location;

                    }
                }
            }
            catch(WebException ex)
            {
                return null;
            }

            return acc;
        }



        public static AccesCodeResponseOneDrive GetAccesCode(string code,string redirect)
        {
            HttpWebRequest req = (HttpWebRequest)HttpWebRequest.Create("https://login.live.com/oauth20_token.srf");
            req.Method = "Post";
            req.ContentType = "application/x-www-form-urlencoded";
            ASCIIEncoding encoding = new ASCIIEncoding();
            string uri = string.Format("client_id={0}&redirect_uri={1}&client_secret={2}&code={3}&grant_type=authorization_code", clientID, redirect, clientSecret, code);
            byte[] bytes = encoding.GetBytes(uri);

            using (Stream str = req.GetRequestStream())
            {
                str.Write(bytes, 0, bytes.Length);
            }
            try {
                using (WebResponse resp = req.GetResponse())
                {
                    using (StreamReader reader = new StreamReader(resp.GetResponseStream()))
                    {
                        string json = reader.ReadToEnd();
                        AccesCodeResponseOneDrive response = JsonConvert.DeserializeObject<AccesCodeResponseOneDrive>(json);
                        return response;
                    }
                }
            }
            catch(Exception ex)
            {
                return null;
            }

        }

        public static AccesCodeResponseOneDrive GetAccesCodeRefresh(string RefreshCode, string redirect)
        {
            HttpWebRequest req = (HttpWebRequest)HttpWebRequest.Create("https://login.live.com/oauth20_token.srf");
            req.Method = "Post";
            req.ContentType = "application/x-www-form-urlencoded";
            ASCIIEncoding encoding = new ASCIIEncoding();
            string uri = string.Format("client_id={0}&redirect_uri={1}&client_secret={2}&refresh_token={3}&grant_type=refresh_token", clientID, redirect, clientSecret, RefreshCode);
            byte[] bytes = encoding.GetBytes(uri);

            using (Stream str = req.GetRequestStream())
            {
                str.Write(bytes, 0, bytes.Length);
            }
            try
            {
                using (WebResponse resp = req.GetResponse())
                {
                    using (StreamReader reader = new StreamReader(resp.GetResponseStream()))
                    {
                        string json = reader.ReadToEnd();
                        AccesCodeResponseOneDrive response = JsonConvert.DeserializeObject<AccesCodeResponseOneDrive>(json);
                        return response;
                    }
                }
            }
            catch (Exception ex)
            {
                return null;
            }

        }



    }

    public class AccesCodeResponseOneDrive
    {
        private string accesToken;

        public string access_token
        {
            get { return accesToken; }
            set { accesToken = value; }
        }

        private string _tokenType;

        public string token_type
        {
            get { return _tokenType; }
            set { _tokenType = value; }
        }

        private string _expires_in;

        public string expires_in
        {
            get { return _expires_in; }
            set { _expires_in = value; }
        }

        private string _refresh_token;

        public string refresh_token
        {
            get { return _refresh_token; }
            set { _refresh_token = value; }
        }


    }

    public class OneDriveAccount
    {
        private string _name;

        public string name
        {
            get { return _name; }
            set { _name = value; }
        }

        private string _first_name;

        public string first_name
        {
            get { return _first_name; }
            set { _first_name = value; }
        }

        private string _last_name;

        public string last_name
        {
            get { return _last_name; }
            set { _last_name = value; }
        }

        private string _link;

        public string link
        {
            get { return _link; }
            set { _link = value; }
        }

        private string _locale;

        public string locale
        {
            get { return _locale; }
            set { _locale = value; }
        }

        private string _gender;

        public string gender
        {
            get { return _gender; }
            set { _gender = value; }
        }

        private string _picture;

        public string picture
        {
            get { return _picture; }
            set { _picture = value; }
        }


    }

    public class ResumeOneDrive
    {
        public string uploadUrl { get; set; }
        public DateTime expirationDateTime { get; set; }
        public IList<string> nextExpectedRanges { get; set; }
    }




}

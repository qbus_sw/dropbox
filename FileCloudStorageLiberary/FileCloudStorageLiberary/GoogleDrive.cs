﻿using Google.Apis.Auth.OAuth2;
using Google.Apis.Drive.v3;
using Google.Apis.Drive.v3.Data;
using Google.Apis.Services;
using Google.Apis.Upload;
using Google.Apis.Util.Store;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace FileCloudStorageLiberary
{


    public class GoogleDrive
    {

        public delegate void ProgressChanged(long value);
         public event ProgressChanged OnProgressChanged;    
        
        private static string clientID = "938440994155-0brk031mq7vmgg7stnp1k1po4jsfjq78.apps.googleusercontent.com";
        private static string clientSecret = "WLxVkRTlL5XJ1HdTerGvMWA9";

        static string[] Scopes = { DriveService.Scope.DriveFile, DriveService.Scope.DriveAppdata, DriveService.Scope.Drive };
        static string ApplicationName = "QbusTestCloudDrive";

        private UserCredential _credential;

        public UserCredential Credential
        {
            get { return _credential; }
            set { _credential = value; }
        }

        private string CloudMapId;

     
        public bool AuthorizeGoogle(string GoogleEmail)
        {
            using (var stream =
           new FileStream("client_secret.json", FileMode.Open, FileAccess.Read))
            {
                string credPath = System.Environment.GetFolderPath(
                    System.Environment.SpecialFolder.Personal);
                credPath = Path.Combine(credPath, ".credentials/"+GoogleEmail);

                Credential = GoogleWebAuthorizationBroker.AuthorizeAsync(
                    GoogleClientSecrets.Load(stream).Secrets,
                    Scopes,
                    "user",
                    CancellationToken.None,
                    new FileDataStore(credPath, true)).Result;
                if(Credential.Token != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }


        public static void UploadFile(string access_token,string filepath,string contentype,string filename,string parentID)
        {
            byte[] filebytes;
            using (System.IO.Stream stream = new FileStream(filepath, FileMode.Open))
            {

                filebytes = new byte[stream.Length];
                stream.Read(filebytes, 0, filebytes.Length);
            }


                HttpWebRequest req = (HttpWebRequest)HttpWebRequest.Create("https://www.googleapis.com/upload/drive/v3/files?uploadType=resumable");
            try
            {
                req.Method = "Post";
                
               
             
                FileUploadGoogle uploadmeta = new FileUploadGoogle();
                uploadmeta.name = filename;
                uploadmeta.mimeType = contentype;
                uploadmeta.parents = new List<string>() { parentID } ;

                req.Headers.Add("Authorization", "Bearer " + access_token);
                req.ContentType = "application/json;charset=UTF-8";
                
                ASCIIEncoding encode = new ASCIIEncoding();
                string bodyJson = JsonConvert.SerializeObject(uploadmeta);
                
                byte[] asciiByte = encode.GetBytes(bodyJson);
                req.ContentLength = asciiByte.Length;
                req.Headers.Add("X-Upload-Content",contentype);
                req.Headers.Add("X-Upload-Length", filebytes.Length+"");
                using (Stream stream = req.GetRequestStream())
                {
                    stream.Write(asciiByte, 0, asciiByte.Length);
                }
                string uri;
                using (WebResponse response = req.GetResponse())
                {
                  uri = response.Headers.Get("Location");

                }
                req = (HttpWebRequest)HttpWebRequest.Create(uri);
                req.Method ="Put";
                JObject obj = new JObject();
                req.ContentType = contentype;
                req.ContentLength = filebytes.Length;
                using (Stream stream = req.GetRequestStream())
                {
                    stream.Write(filebytes, 0, filebytes.Length);
                }
                using (WebResponse response = req.GetResponse())
                {
                    using (StreamReader respStream = new StreamReader(response.GetResponseStream()))
                    {
                        string json = respStream.ReadToEnd();
                    }

                }

            }
            catch(WebException ex)
            {

            }
        }


        public static List<string> GetList(string acces_token,string parentId)
        {
            HttpWebRequest req = (HttpWebRequest)HttpWebRequest.Create("https://www.googleapis.com/drive/v3/files?access_token="+acces_token+"&q=trashed%3Dfalseand%27"+parentId+"%27%20in%20parents");
            try
            {
                List<string> lijstString = new List<string>();
                using (WebResponse response = req.GetResponse())
                {
                    using (StreamReader reader = new StreamReader(response.GetResponseStream()))
                    {

                        string lees = reader.ReadToEnd();
                        fileresponse list = JsonConvert.DeserializeObject<fileresponse>(lees);
                        foreach(file NameFile in list.files)
                        {
                            lijstString.Add(NameFile.name);
                        }
                        return lijstString;
                    }
                }
               
            }
            catch (WebException ex)
            {
                return null;
            }
        }

        public static string MaakCloudFolder(string access_token)
        {
            string bestaat = BestaatCloudFolder(access_token);
            if (bestaat !="0")
            {
                return bestaat;
            }
             
            HttpWebRequest req = (HttpWebRequest)HttpWebRequest.Create("https://www.googleapis.com/drive/v3/files");
            JObject obj = new JObject();

            obj.Add("name", "QbusCloudTest");
            obj.Add("mimeType", "application/vnd.google-apps.folder");
            string json = obj.ToString();
            ASCIIEncoding encoding = new ASCIIEncoding();
            byte[] values = encoding.GetBytes(json);
            req.Method = "POST";
            // Set the content type of the data being posted.
            req.ContentType = "application/json";
            req.Headers.Add("Authorization", "Bearer " + access_token);
            // Set the content length of the string being posted.
            req.ContentLength = values.Length;

            using (Stream body = req.GetRequestStream())
            {
                body.Write(values, 0, values.Length);
            }


                try {
                    using (WebResponse response = req.GetResponse())
                    {
                        StreamReader reader = new StreamReader(response.GetResponseStream());
                        string responseJson = reader.ReadToEnd();
                        file datafile = JsonConvert.DeserializeObject<file>(responseJson);
                        return datafile.id;
                    }
                }
                catch (WebException ex)
                {
                    return "0";
                }

        }

        public static string BestaatCloudFolder(string access_token)
        {

            HttpWebRequest req = (HttpWebRequest)HttpWebRequest.Create("https://www.googleapis.com/drive/v3/files?access_token=" + access_token + "&q=mimeType%3D'application%2Fvnd.google-apps.folder'+and+trashed%3Dfalse");
            try
            {
                List<string> lijstString = new List<string>();
                using (WebResponse response = req.GetResponse())
                {
                    using (StreamReader reader = new StreamReader(response.GetResponseStream()))
                    {
                        string json = reader.ReadToEnd();
                        fileresponse temp = JsonConvert.DeserializeObject<fileresponse>(json);
                        var item = temp.files.Single(p => p.name == "QbusCloudTest");
                        return item.id;
                    }
                }
            }
            catch(Exception ex)
            {
                return "0";
            }
      }


        public static string AccesTokenUrl = "https://www.googleapis.com/oauth2/v4/token?";
        public static Dictionary<string,string> GetAccesToken(string code, string redricturi)
        {
            //
            string url = "access_type=offline&approval_prompt=force&code=" + code + "&client_id=" + clientID + "&client_secret=" + clientSecret + "&redirect_uri="+redricturi + "&grant_type=authorization_code";
            ASCIIEncoding encoding = new ASCIIEncoding();
            byte[] uri = encoding.GetBytes(url);

            //request maken
            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(AccesTokenUrl);
            request.Method = "POST";
            request.ContentType = "application/x-www-form-urlencoded";

            request.ContentLength = uri.Length;
            Stream newStream = request.GetRequestStream();
            // Send the data.
            newStream.Write(uri, 0, uri.Length);
            newStream.Close();
            try
            {
                using (WebResponse resp = request.GetResponse())
                {
                    StreamReader reader = new StreamReader(resp.GetResponseStream());
                    string response = reader.ReadToEnd();
                    AccesToken Des = JsonConvert.DeserializeObject<AccesToken>(response);
                    Dictionary<string, string> temp = new Dictionary<string, string>();
                    temp.Add("access_token", Des.access_token);
                    temp.Add("token_type", Des.token_type);
                    temp.Add("expires_in", Des.expires_in);
                    temp.Add("id_token", Des.id_token);
                    return temp;
                }

            }
            catch (WebException ex)
            {
                return null;
            }
        }

        public static Dictionary<string, string> RefreshAccesToken(string RefreshToken)
        {
            //
            string url =  "client_id=" + clientID + "&client_secret=" + clientSecret + "&id_token=" + RefreshToken + "&grant_type=refresh_token";
            ASCIIEncoding encoding = new ASCIIEncoding();
            byte[] uri = encoding.GetBytes(url);

            //request maken
            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(AccesTokenUrl);
            request.Method = "POST";
            request.ContentType = "application/x-www-form-urlencoded";

            request.ContentLength = uri.Length;
            Stream newStream = request.GetRequestStream();
            // Send the data.
            newStream.Write(uri, 0, uri.Length);
            newStream.Close();
            try
            {
                using (WebResponse resp = request.GetResponse())
                {
                    StreamReader reader = new StreamReader(resp.GetResponseStream());
                    string response = reader.ReadToEnd();
                    AccesToken Des = JsonConvert.DeserializeObject<AccesToken>(response);
                    Dictionary<string, string> temp = new Dictionary<string, string>();
                    temp.Add("access_token", Des.access_token);
                    temp.Add("token_type", Des.token_type);
                    temp.Add("expires_in", Des.expires_in);
                  
                    return temp;
                }

            }
            catch (WebException ex)
            {
                return null;
            }
        }

        private DriveService _service;

        public DriveService Service
        {
            get { return _service; }
            set { _service = value; }
        }

        public string GetAuthCodeUrl(string redirecturi)
        {
            
            return  "https://accounts.google.com/o/oauth2/v2/auth?" + "&scope=https://www.googleapis.com/auth/drive%20https://www.googleapis.com/auth/drive.file%20profile" + "&redirect_uri=" + redirecturi + "&response_type=code&client_id=" + clientID;

        }

       


        public void CreateService()
        {
            
            _service = new DriveService(new BaseClientService.Initializer()
            {
                HttpClientInitializer = Credential,
                ApplicationName = ApplicationName,
            });

        }


        Dictionary<string, string> FileList = new Dictionary<string, string>();

        public void MaakFileList()
        {
            // Define parameters of request.
            FilesResource.ListRequest listRequest = Service.Files.List();
           
            listRequest.Fields = "nextPageToken, files(id, name)";

            // List files.
            IList<Google.Apis.Drive.v3.Data.File> files = listRequest.Execute()
                .Files;
            Console.WriteLine("Files:");
            if (files != null && files.Count > 0)
            {
                foreach (var file in files)
                {
                    Console.WriteLine("{0} ({1})", file.Name, file.Id);
                }
            }
            else
            {
                Console.WriteLine("No files found.");
            }
            Console.Read();

        



    }



        public  void MaakCloudMap()
        {
            

            string nextPageToken = null;
            object temp = null;
            do
            {
                var listRequest = Service.Files.List();
                listRequest.Q = "mimeType = 'application/vnd.google-apps.folder' and trashed=false";
                listRequest.Spaces = "drive";
                listRequest.Fields = "nextPageToken, files(id, name)";
                listRequest.OauthToken = Credential.Token.AccessToken;
                listRequest.PageToken = nextPageToken;
                  var list=   listRequest.Execute();

                if (list != null)
                {
                    try {
                        temp = list.Files.Single(s => s.Name == ApplicationName);
                        var item = (Google.Apis.Drive.v3.Data.File)temp;
                        CloudMapId = item.Id;
                    }
                    catch
                    {
                        temp = null;
                    }

                }
            }
            while (nextPageToken != null);

             if (temp == null)
            {
                var fileMetaData = new Google.Apis.Drive.v3.Data.File();
                fileMetaData.MimeType = "application/vnd.google-apps.folder";
                fileMetaData.Name = ApplicationName;

                FilesResource.CreateRequest createReq = Service.Files.Create(fileMetaData);
                createReq.Fields = "id";
                var file = createReq.Execute();
                
                CloudMapId = file.Id;
            }
            
           
        }


        public async Task<bool> uploadFile(string filepath)
        {
            string split = filepath.Remove(0, filepath.LastIndexOf('\\'));
            split = split.TrimStart('\\');
            string[] exeAndName = split.Split('.');
            using (FileStream stream = new FileStream(filepath, FileMode.Open))
            {
                Google.Apis.Drive.v3.Data.File body = new Google.Apis.Drive.v3.Data.File();
                body.Name = exeAndName[0];

                body.Parents = new List<string>() { CloudMapId };
                switch(exeAndName[1])
                {
                    case "txt": { body.MimeType = "text/plain";  break; }
                    case "doc": { body.MimeType = "text/plain"; break; }
                    case "docx": { body.MimeType = "text/plain"; break; }
                    case "log": { body.MimeType = "text/plain"; break; }
                    case "csv": { body.MimeType = "text/plain"; break; }
                    case "json": { body.MimeType = "text/plain"; break; }
                    case "xml": { body.MimeType = "text/plain"; break; }
                    case "png": { body.MimeType = "application/vnd.google-apps.photo"; break; }
                    case "jpeg": { body.MimeType = "application/vnd.google-apps.photo"; break; }
                    case "gif": { body.MimeType = "application/vnd.google-apps.photo"; break; }
                    case "bmp": { body.MimeType = "application/vnd.google-apps.photo"; break; }
                    case "tiff": { body.MimeType = "application/vnd.google-apps.photo"; break; }
                    case "mp4": { body.MimeType = "application/vnd.google-apps.video"; break; }
                    case "avi": { body.MimeType = "application/vnd.google-apps.video"; break; }
                    case "flv": { body.MimeType = "application/vnd.google-apps.video"; break; }
                    case "wmv": { body.MimeType = "application/vnd.google-apps.video"; break; }
                    case "mov": { body.MimeType = "application/vnd.google-apps.video"; break; }
                }
               FilesResource.CreateMediaUpload Upload = new FilesResource.CreateMediaUpload(Service,body,stream,GetDescription(filepath));
                Upload.ProgressChanged += Upload_ProgressChanged;
             

              IUploadProgress status =  await Upload.UploadAsync();

                if(status.Status == UploadStatus.Completed)
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }

        }

       
        private void Upload_ProgressChanged(IUploadProgress obj)
        {
            OnProgressChanged(obj.BytesSent);
        }


        public static string ReadDefaultValue(string regKey)
        {
            using (var key = Microsoft.Win32.Registry.ClassesRoot.OpenSubKey(regKey, false))
            {
                if (key != null)
                {
                    return key.GetValue("") as string;
                }
            }
            return null;
        }

        public static string GetDescription(string ext)
        {
            if (ext.StartsWith(".") && ext.Length > 1) ext = ext.Substring(1);

            var retVal = ReadDefaultValue(ext + "file");
            if (!String.IsNullOrEmpty(retVal)) return retVal;


            using (var key = Microsoft.Win32.Registry.ClassesRoot.OpenSubKey("." + ext, false))
            {
                if (key == null) return "";

                using (var subkey = key.OpenSubKey("OpenWithProgids"))
                {
                    if (subkey == null) return "";

                    var names = subkey.GetValueNames();
                    if (names == null || names.Length == 0) return "";

                    foreach (var name in names)
                    {
                        retVal = ReadDefaultValue(name);
                        if (!String.IsNullOrEmpty(retVal)) return retVal;
                    }
                }
            }

            return "";
        }
    }

    class AccesToken
    {
        private string accesToken;

        public string access_token
        {
            get { return accesToken; }
            set { accesToken = value; }
        }

        private string refreshToken;

        public string id_token
        {
            get { return refreshToken; }
            set { refreshToken = value; }
        }

        private string expiresin;

        public string expires_in
        {
            get { return expiresin; }
            set { expiresin = value; }
        }

        private string _tokenType;

        public string token_type
        {
            get { return _tokenType; }
            set { _tokenType = value; }
        }

    }

   public class fileresponse
    {
        private string _kind;

        public string kind
        {
            get { return _kind; }
            set { _kind = value; }
        }

        private List<file> _files;

        public List<file> files
        {
            get { return _files; }
            set { _files = value; }
        }


    }

    public class FileUploadGoogle
    {
        private string _name;

        public string name
        {
            get { return _name; }
            set { _name = value; }
        }

        private string _mimeType;

        public string mimeType
        {
            get { return _mimeType; }
            set { _mimeType = value; }
        }


        private List<string> _parents;

        public List<string> parents
        {
            get { return _parents; }
            set { _parents = value; }
        }


    }

    public class filemetaGoogle
    {
        private string _kind = "drive#fileLink";

        public string kind
        {
            get { return _kind; }
            set { _kind = value; }
        }
        private string _id;

        public string id
        {
            get { return _id; }
            set { _id = value; }
        }

    }

   public class file
    {
        private string _kind;

        public string kind
        {
            get { return _kind; }
            set { _kind = value; }
        }

        private string _id;

        public string id
        {
            get { return _id; }
            set { _id = value; }
        }
        private string _name;

        public string name
        {
            get { return _name; }
            set { _name = value; }
        }

        private string _mimetype;

        public string mimetype
        {
            get { return _mimetype; }
            set { _mimetype = value; }
        }

    }
}

﻿

using System;

namespace FileCloudStorageLiberary.OneDriveModels
{
    public class LastModifiedBy
    {
        public Application application{ get; set; }
         public User user{ get; set; }
         public OneDriveSync oneDriveSync{ get; set; }
        
    }
}
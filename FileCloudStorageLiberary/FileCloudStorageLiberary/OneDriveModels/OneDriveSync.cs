﻿using Newtonsoft.Json;

namespace FileCloudStorageLiberary.OneDriveModels
{
    public class OneDriveSync
    {
        [JsonProperty(" @odata.type")]
        public string data_type { get; set; }
        public string id { get; set; }
    }
}
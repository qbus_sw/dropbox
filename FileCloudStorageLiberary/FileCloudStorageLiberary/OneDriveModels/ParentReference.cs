﻿namespace FileCloudStorageLiberary.OneDriveModels
{
    public class ParentReference
    {
        public string driveId { get; set; }
        public string id { get; set; }
        public string path { get; set; }
    }
}
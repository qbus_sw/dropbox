﻿namespace FileCloudStorageLiberary.OneDriveModels
{
    public class CreatedBy
    {
        public Application application { get; set; }
        public User user { get; set; }
        public OneDriveSync oneDriveSync
        {
            get; set;
        }
    }
}
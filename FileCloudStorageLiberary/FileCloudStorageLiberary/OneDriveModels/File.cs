﻿namespace FileCloudStorageLiberary.OneDriveModels
{
    public class File
    {
        public Hashes hashes { get; set; }
        public string mimeType { get; set; }
    }
}
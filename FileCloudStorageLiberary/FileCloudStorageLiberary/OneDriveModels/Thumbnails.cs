﻿namespace FileCloudStorageLiberary.OneDriveModels
{
    public class Thumbnails
    {
        public Large large { get; set; }
        public Medium medium { get; set; }
        public Small small { get; set; }
        public Source source { get; set; }
    }
}
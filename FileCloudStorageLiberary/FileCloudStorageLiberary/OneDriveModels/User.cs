﻿namespace FileCloudStorageLiberary.OneDriveModels
{
    public class User
    {
        public string displayName { get; set; }
        public string id { get; set; }
        public Thumbnails thumbnails { get; set; }
    }
}
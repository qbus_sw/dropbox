﻿namespace FileCloudStorageLiberary.OneDriveModels
{
    public class Large
    {
        public int height { get; set; }
        public string url { get; set; }
        public int width { get; set; }
    }
}
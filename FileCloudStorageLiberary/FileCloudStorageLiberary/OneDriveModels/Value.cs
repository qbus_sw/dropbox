﻿
using Newtonsoft.Json;
using System;

namespace FileCloudStorageLiberary.OneDriveModels
{
    public class Value
    {
            [JsonProperty("@content.downloadUrl")]
            public string content_downloadUrl { get; set; }
            public CreatedBy createdBy { get; set; }
            public DateTime createdDateTime { get; set; }
            public string cTag { get; set; }
            public string eTag { get; set; }
            public string id { get; set; }
            public LastModifiedBy lastModifiedBy { get; set; }
            public DateTime lastModifiedDateTime { get; set; }
            public string name { get; set; }
            public ParentReference parentReference { get; set; }
            public int size { get; set; }
            public string webUrl { get; set; }
            public File file { get; set; }
            public FileSystemInfo fileSystemInfo { get; set; }
        
    }
}
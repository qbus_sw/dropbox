﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileCloudStorageLiberary.OneDriveModels
{
   public class FileLijstOneDrive
    {
        [JsonProperty("@odata.context")]
        public string odata_context { get; set; }
        public IList<Value> value { get; set; }
    }
}

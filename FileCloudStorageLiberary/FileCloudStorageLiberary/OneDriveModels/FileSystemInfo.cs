﻿using System;

namespace FileCloudStorageLiberary.OneDriveModels
{
    public class FileSystemInfo
    {
        public DateTime createdDateTime { get; set; }
        public DateTime lastModifiedDateTime { get; set; }
    }
}
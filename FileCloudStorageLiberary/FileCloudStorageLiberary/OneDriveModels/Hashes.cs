﻿namespace FileCloudStorageLiberary.OneDriveModels
{
    public class Hashes
    {
        public string crc32Hash { get; set; }
        public string sha1Hash { get; set; }
    }
}
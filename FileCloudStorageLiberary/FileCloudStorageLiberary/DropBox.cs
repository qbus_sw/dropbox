﻿using Dropbox.Api;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace FileCloudStorageLiberary
{
    public class DropBox
    {




        public delegate void FileUploadFinishedEvent();
        public event FileUploadFinishedEvent OnUploadFinished;

        public delegate void AccountConntectedFinished();
        public event AccountConntectedFinished OnAccountConnected;

        private void UpdateStatus()
        {
            OnUploadFinished();
        }

        private static string secret = "s768775mjvwf8uj";
        private static string clientID = "ts7ojw5nftdwmac";

        private string _accesToken = "tdcIz1uVoDQAAAAAAAAL-Yg4pamS-4zSV6iHTRQZPoVq2C_7vv_aD2FMB-f1Kb72";

        public string AccesToken
        {
            get { return _accesToken; }
            set { _accesToken = value; }
        }

        private string _redrictUri;

        public string RedirectUri
        {
            get { return _redrictUri; }
            set { _redrictUri = value; }
        }

        private DropboxClient _client;




        public string ConnectDropBoxString()
        {

            //RedirectUri = @"https://www.dropbox.com/1/oauth2/redirect_receiver";
            string clientID = "ts7ojw5nftdwmac";
            var uri = string.Format(
      @"https://www.dropbox.com/1/oauth2/authorize?response_type=code&redirect_uri={0}&client_id={1}",
      RedirectUri, clientID);


            return uri;

        }

        private string _userName;

        public string UserName
        {
            get { return _userName; }
            set { _userName = value; }
        }

        private string _userEmail;

        public string UserEmail
        {
            get { return _userEmail; }
            set { _userEmail = value; }
        }

        private Dropbox.Api.Files.UploadSessionStartResult _sessionStartResult;

        public Dropbox.Api.Files.UploadSessionStartResult SessionStartResult
        {
            get { return _sessionStartResult; }
            set { _sessionStartResult = value; }
        }

        private Dropbox.Api.Files.UploadSessionCursor _cursor;

        public Dropbox.Api.Files.UploadSessionCursor Cursor
        {
            get
            {

                return _cursor;
            }
            set { _cursor = value; }
        }



        public async void ConnectWithoutAuth()
        {
            _client = new DropboxClient(AccesToken);

            var user = await _client.Users.GetCurrentAccountAsync();
            
            UserEmail = user.Email;
            UserName = user.Name.DisplayName;
            OnAccountConnected();   
        }

        public static string AccesTokenUrl = "https://api.dropboxapi.com/1/oauth2/token";

        public static Dictionary<string,string> GetAccesToken(string token,string redricturi)
        {

            string url = "code=" + token + "&grant_type=authorization_code&client_id=" + clientID + "&client_secret=" + secret + "&redirect_uri=" + redricturi;
            ASCIIEncoding encoding = new ASCIIEncoding();
            byte[] uri = encoding.GetBytes(url);

            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(AccesTokenUrl);
            request.Method = "POST";
            request.ContentType = "application/x-www-form-urlencoded";

            request.ContentLength = uri.Length;
            Stream newStream = request.GetRequestStream();
            // Send the data.
            newStream.Write(uri, 0, uri.Length);
            newStream.Close();
            try
            {
                using (WebResponse resp = request.GetResponse())
                {
                    StreamReader reader = new StreamReader(resp.GetResponseStream());
                    string response = reader.ReadToEnd();
                    AccesTokenClass Des = JsonConvert.DeserializeObject<AccesTokenClass>(response);
                    Dictionary<string,string> temp = new Dictionary<string, string>();
                    temp.Add("access_token", Des.access_token);
                    temp.Add("token_type", Des.token_type);
                    temp.Add("uid", Des.uid);
                    return temp;
                }
               
            }
            catch(WebException ex)
            {
                return null;
            }
        }

        public async Task ConnectWithAuth(string accestoken)
        {
            _client = new DropboxClient(accestoken);

            var user = await _client.Users.GetCurrentAccountAsync();

            UserEmail = user.Email;
            UserName = user.Name.DisplayName;
            OnAccountConnected();
        }

        private Dropbox.Api.Files.FileMetadata _metaData;

        public Dropbox.Api.Files.FileMetadata MetaData
        {
            get { return _metaData; }
            set { _metaData = value; UpdateStatus(); }
        }


       


        public async Task UploadFile(string filelocation)
        {
           string filename = filelocation.Remove(0,filelocation.LastIndexOf('\\')+1);
            filename = string.Format(
                CultureInfo.InvariantCulture,filename);
            
            int index = 0;
            ulong oldIndex = 0;
            bool firsttime = true;
            using (FileStream str = new FileStream(filelocation, FileMode.Open))
            {
                long lenght = str.Length;
                
                while (index < str.Length)
                {
                    byte[] buffer = new byte[CalculateArraylenght(lenght)];

                    if(lenght > 50000000)
                    {
                        lenght -= 50000000;
                    }

                    int bytesread = str.Read(buffer, 0, buffer.Length);
                    if (bytesread == 0)
                    {
                        break;
                    }
                    index += bytesread;

                    if (index != 0)
                    {
                        //char[] chars = UTF8Encoding.UTF8.GetChars(buffer);
                        using (MemoryStream memstr = new MemoryStream(buffer))
                        {

                            if (firsttime)
                            {
                                SessionStartResult = await _client.Files.UploadSessionStartAsync(memstr);
                                firsttime = false;
                            }
                            else
                            {

                                Cursor = new Dropbox.Api.Files.UploadSessionCursor(SessionStartResult.SessionId, oldIndex);

                                await _client.Files.UploadSessionAppendAsync(Cursor, memstr);
                            }



                        }
                    }
                    oldIndex = (ulong)index;

                }

                using (MemoryStream memstr = new MemoryStream())
                {
                    try {
                        Dropbox.Api.Files.WriteMode var=   new Dropbox.Api.Files.WriteMode();
                        
                       Dropbox.Api.Files.CommitInfo info = new Dropbox.Api.Files.CommitInfo("/" + filename, var.AsOverwrite,true);

                        Cursor = new Dropbox.Api.Files.UploadSessionCursor(SessionStartResult.SessionId, oldIndex);
                        MetaData = await _client.Files.UploadSessionFinishAsync(Cursor, info, memstr);
                        return;
                    }
                    catch(Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                    }
                }


            }
        }

        public int CalculateArraylenght(long lenght)
        {
            if (lenght > 50000000)
            {
                return 50000000;
            }
            else
            {
                return (int)lenght;
            }

                
        }
    }
    class AccesTokenClass
    {
        private string accesToken;

        public string access_token
        {
            get { return accesToken; }
            set { accesToken = value; }
        }

        private string _tokenType;

        public string token_type
        {
            get { return _tokenType; }
            set { _tokenType = value; }
        }

        private string _uid;

        public string uid
        {
            get { return _uid; }
            set { _uid = value; }
        }


    }
}
